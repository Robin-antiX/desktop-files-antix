# desktop-files-antiX

Script for single- and multiuser desktop file set management

## Description
This script switches beween the default .desktop file set and the
antiX .desktop file set provided by the `antix-desktop-files` package.
Once installed on a system the antiX .desktop file set resides in
the `/usr/share/antiX-desktop-files` folder, and this script creates
or removes symbolic links to the hidden .local/share/applications
subfolder within the user's home folder and lets the users menu get
refreshed. Existing files of the same name present in users folder are
saved to backup copies. The unset options restore these backup copies
while removing the symbolic links created by the set options.

This allows to switch bewtween different sets of .desktop files on the fly.

## Installation
Script will be part of antix-desktop-files package, which is an antiX community project. An additional GUI will come also, meant to get integrated into antiX control center for easy access.

## Usage
Script accepts some command line options:
- --set-single          Switches to the antiX desktop files.
- --unset-single        Switches back to the default desktop files provided by the programs itself.
- --set-global          Switches all users of a system to the antiX desktop files.
- --unset-global        Switches all users back to default desktop files.
- --help                Shows the script usage and information.
All global options need sudo or root for system wide access of files.

## Support
[antiX-Forums](http://www.antixforum.com)

## Roadmap
bughunting, bugfixing, packaging together with the .desktop files from transifex. 

## Contributing
Everybody skilled to help improving this community script and package is welcome.

## Authors and acknowledgment
This is an antiX community project

## License
GPL v3.

## Project status
In progress.
